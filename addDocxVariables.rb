require 'zip'
require 'optparse'

# Docx data file. Susceptible to change with word .docx estructure
DOC_FILE = 'word/document.xml'

OPT_MSG = 'Optional input file. file.docx by default'
OUT_MSG = 'Optional output file. output.docx by default'

# Replace current file if exists
Zip.on_exists_proc = true

# Use unicode names (for Windows <7)
Zip.unicode_names = true

# ARGV options
options = {}
OptionParser.new do |opts|
  options[:ipt] = false
  opts.on('-i', '--optional [INPUT]', OPT_MSG) do |ipt|
    options[:input] = ipt || 'file.docx'
  end
  opts.on('-o', '--output [OUTPUT]', OUT_MSG) do |opt|
    options[:output] = opt || 'output.docx'
  end
end.parse!

# No ARGV default options
options[:input] ||= 'file.docx'
options[:output] ||= 'output.docx'

# --------------------------------------
# MODIFY THIS METHOD TO MODIFY DOCX FILE
def modify_method(doc_str)
  # E.G:
  # doc_str.gsub!(NAME, Bob)
  # Will change the tag "NAME" and set as "Bob"
  doc_str
end
# --------------------------------------

# Main method
def modify_docx(options)
  # Input reading
  docs = Dir[options[:input]]
  unless docs.size == 1
    puts 'Input file not found'
    Kernel.exit
  end

  # Take doc data
  zip_file_data = {}
  doc_data = nil
  Zip::File.open(options[:input]) do |zip_file|
    zip_file.each do |e|
      zip_file_data[e.name] = e.get_input_stream.read if (e.name != DOC_FILE)
      doc_data = zip_file.read(DOC_FILE).force_encoding('utf-8')
    end
  end

  # Customizable method call
  doc_data = modify_method(doc_data)

  # Doc writing
  buffer = Zip::OutputStream.write_buffer do |out|
    data_array = zip_file_data.keys
    data_array.each do |name|
      out.put_next_entry(name)
      out.write zip_file_data[name]
    end

    out.put_next_entry(DOC_FILE)
    out.write doc_data
  end
  File.open(options[:output], 'wb') { |f| f.write(buffer.string) }
end

modify_docx(options)
