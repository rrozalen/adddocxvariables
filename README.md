# AddDocxvariables Readme #


This script allows you to modify any string "tag" and put any text in his place. The string must be in the input docx file or it won't be effective. This can be used to fill automatically docx forms or generate answers, data, etc.


---- USAGE: ----

ruby addDocxVariables.rb - i [inputDocxName] -o [outputDocxName]

-i inputDocxName DEFAULT: 'file.docx'

-o output DEFAULT: 'output.docx'

i.e: ruby addDocxVariables.rb -i 'plantilla.docx' -o 'nuevo.docx'

All arguments are optional


You have to modify the method 'modifyMethod' to change docx strings. An example is given in the ruby file.